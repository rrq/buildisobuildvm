#!/bin/zsh

typeset -A QEMUS

QEMUS[amd64]=qemu-system-x86_64
QEMUS[i386]=qemu-system-i386
QEMUS[arm64]=qemu-system-aarch64
QEMUS[armhf]=qemu-system-arm

ARCH=${1%-w}
ARCH=${ARCH%-efi}

QEMU=$QEMUS[$ARCH]
DISK=$2
if [ -z "${3##usb=*}" ] ; then
    USBCD="${3#usb=}"
else
    CD=$3
fi

if [ -z "$QEMU" ] || [ ! -r "$DISK" ] ; then
    echo "Usage: <arch> <disk>" >&2
    echo "where arch is one of: ${(k)QEMUS}"
    exit 1
fi

case "$ARCH" in
    armhf)
	MACHINE=( -machine virt )
	;;
    arm64)
	MACHINE=( -machine virt -cpu cortex-a57 )
	;;
    *)
	MACHINE=( -machine pc,accel=kvm -cpu host )
	;;
esac

# No display if no CD or USBCD
if [ -z "$CD$USBCD" ] ; then
    BOOTDIR="${DISK%.img}-boot"
    if [ -d "$BOOTDIR" ] ; then
	BOOT=(
	    -kernel $BOOTDIR/vmlinuz -initrd $BOOTDIR/initrd.img
	    -append "root=/dev/vda1"
	)
    else
	BOOT=( -boot order=c )
    fi
    [ -z "${(M)1%-w}" ] && BOOT+=( -vnc none )
elif [ -z "$CD" ] ; then
    BOOT=( -boot menu=on )
else
    BOOT=( -boot order=cd,once=d )
fi

[ -z "${(M)1%-efi}" ] || BOOT+=( -bios /usr/share/qemu/OVMF.fd )

# compute MAC address from script path adn disk image
MAC=( $(readlink -f $0/${DISK}|md5sum|sed 's/\(..\)/ \1/g') )
MAC="04$(printf ":%s" ${MAC[@]:3:5})"
NET=${NET-vde,sock=/tmp/vde.ctl}

ARG=(
    -name ${DISK}_build
    $MACHINE -smp cpus=4,cores=4 -m 4G
    -serial mon:stdio -echr 0x1c $BOOT
    -vga std
    #-vga none
    -drive index=0,file=${DISK},media=disk,format=raw
    -net nic,macaddr=$MAC -net $NET
    -device nec-usb-xhci,id=usbhub
    -soundhw ac97
)

if [ ! -z "$CD" ] ; then
    ARG+=( -drive index=1,file=$CD,media=cdrom )
fi
if [ ! -z "$USBCD" ] ; then
    ARG+=(
	-drive if=none,id=usbmedia,file=$USBCD,media=cdrom
	-device usb-storage,bus=usbhub.0,drive=usbmedia
    )
fi

echo QEMU_AUDIO_DRV=alsa exec $QEMU $ARG | sed "s/ -/\n-/g"
QEMU_AUDIO_DRV=alsa exec $QEMU $ARG

#!/bin/zsh
#
# Bring up the control dialog for a bootstrap run

FIELDS=(
    DIST ARCH PROXY SHARE SHARETYPE WHO WHOID TARGET VARIANT EXCLUDE SOURCE
)

typeset -A FIELD
for F in $FIELDS ; do FIELD[$F]="$(eval echo \$$F)" ; done

N=1
DIALOG=(
    --form "Configuration variables" $((${#FIELDS}+6)) 65 ${#FIELDS}
)
for F in $FIELDS ; do
    V="$FIELD[$F]"
    DIALOG+=( $F $N 1 "$V" $N 10 49 49 )
    N=$((N+1))
done
dialog --title "Set up for running bootstrap.sh" \
       --hfile bootstrap-help.txt --hline "Use ^E for help" \
       --stdout "${(@)DIALOG}" | \
    for F in $FIELDS ; do read $F || return 1 ; done

for F in $FIELDS ; do echo "$F='$(eval echo \$$F)'" ; done

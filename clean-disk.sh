#!/bin/zsh
#
# Create a simple raw virtual disk with nominated partition and swap sizes
# Usage: filename partition-size swap-size

DISK=$1
FSSZ=${2-2G}
SWAPSZ=${3-200M}
ROOTFS=$4

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] ; then
    echo "Usage: filename partition-size swap-size [rootfs]" >&2
    exit 1
fi

if [ -r "$1" ] ; then
    ./clean-disk-dialog.sh $DISK || exit 1
fi

# Translate a size argument, with optional G or M, into block size count
function blocksize() {
    if [ -z "${1:#*G}" ] ; then
       echo $(( ${1%G} * 2097152 ))
    elif [ -z "${1:#*M}" ] ; then
	echo $(( ${1%M} * 2048 ))
    else
	echo $(( ${1} / 512 ))
    fi
}

FSSZ=$(blocksize $FSSZ)
SWAPSZ=$(blocksize $SWAPSZ)
EXTRA=2048
cat <<EOF
FSSZ=$FSSZ blocks
SWAPSZ=$SWAPSZ blocks
EXTRA=2048 blocks
TOTAL=$((FSSZ+SWAPSZ+EXTRA)) blocks
EOF

if [ -r $DISK ] ; then
    rm $DISK || exit 1
fi

dd if=/dev/zero of=$1 bs=512 count=0 seek=$(( FSSZ + SWAPSZ + EXTRA ))

[ $SWAPSZ = 0 ] || \
    SWAPINFO="${1}2 : start= $((EXTRA+FSSZ)), size= $SWAPSZ, type=82"
cat <<EOF | sfdisk $1
label: dos
device: $1
unit: sectors

${1}1 : start= $EXTRA, size= $FSSZ, type=83, bootable
$([ $SWAPSZ = 0 ] || \
   echo "${1}2 : start= $((EXTRA+FSSZ)), size= $SWAPSZ, type=82")
EOF
LOOP=$(sudo losetup --show -f $1)
sudo mkfs.ext4 -LROOTFS ${LOOP}p1
[ $SWAPSZ = 0 ] || sudo mkswap -LSWAP ${LOOP}p2

if [ -n "$ROOTFS" ] ; then
    echo "Unpacking $ROOTFS"
    sudo mount ${LOOP}p1 FS
    gunzip < $ROOTFS | ( cd FS ; sudo cpio -i )
    echo "Inspect FS/ and exit"
    zsh
    echo "... Now finishing clean-disk.sh $*"
    MBR=FS/usr/lib/EXTLINUX/altmbr.bin
    if [ -r $MBR ] ; then
	echo "Installing $MBR"
	sudo extlinux -i FS
	#cat $MBR | dd of=$DISK bs=440 count=1 conv=notrunc
	printf '\1' | cat $MBR - | dd of=$DISK bs=440 count=1 conv=notrunc
    fi
    sudo umount FS
fi
sudo losetup -d $LOOP

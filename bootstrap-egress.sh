## These are egress commands that get passed into a chroot bash for
## finalizing the set up of the builder virtual machine image, but
## uppercase environment variables are first replaced with contents.

# Set up root password
( echo toor ; echo toor ) | /usr/bin/passwd -q

# Host name and networking
cat <<END >> /etc/network/interfaces
auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp
END

# Configure apt
mkdir -p /etc/apt/apt.conf.d
echo 'APT::Install-Recommends "0";' > /etc/apt/apt.conf.d/05norecommends
echo 'APT::Sandbox::Seccomp "0";' > /etc/apt/apt.conf.d/10nosandbox
if [ -n "$PROXY" ] ; then
    echo Acquire::http::proxy '"'$PROXY'";' > /etc/apt/apt.conf.d/02proxy
fi

# Prepare script to install the extra post-boot packages
cat <<END > /root/postboot.sh
#!/bin/bash
POSTBOOTLOG="/root/postboot.sh.log-\$(date +%Y%m%d-%H%M%S)"
echo "POSTBOOTLOG=\$POSTBOOTLOG"
exec 2> \$POSTBOOTLOG
[ -e /etc/rc.local.0 ] && mv /etc/rc.local.0 /etc/rc.local
set -x
apt-get update
$(for f in $POSTBOOT ; do 
    echo "apt-get -y install --no-install-recommends $f || exit"
done)
END
chmod a+x /root/postboot.sh
#mv /etc/rc.local /etc/rc.local.0
#ln -s /root/postboot.sh /etc/rc.local

# Set up file sharing on /share from SHARE and optional SHARETYPE
if [ -n "$SHARE" ] ; then
    SHARETYPE=$SHARETYPE
    mkdir -p /share
    cat <<END > /etc/fstab
/dev/sda1 / ext4 defaults,noatime 0 1
$SHARE /share ${SHARETYPE-nfs} defaults,user,exec,auto,soft 0 0
END
    echo "mount /share" >> /root/postboot.sh
fi

# Extend the postboot script for debian-installer via a share set up
# uses DIST DIPKGS WHO WHOID
cat <<END >> /root/postboot.sh
cat << DEND >> /etc/apt/sources.list
deb http://deb.devuan.org/merged $DIST contrib non-free
deb http://deb.devuan.org/merged $DIST main/debian-installer
DEND
apt-get update
$(for p in $DIPKGS ; do
    echo "apt-get -y install --no-install-recommends $p || exit"
done)
END
if [ ! -z "$WHO" ] ; then
    cat << END >> /root/postboot.sh
EOF
adduser --uid $WHOID --disabled-password --gecos $WHO $WHO
ln -s /share/debian-installer /home/$WHO/debian-installer
su - $WHO -c 'cd debian-installer/build ; make reallyclean'
END
fi

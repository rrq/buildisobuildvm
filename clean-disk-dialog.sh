#/bin/zsh

M="\\n    Disk $1 will be erased!\\n    Is that really what you want ?"
dialog --stdout --title "!! WARNING !!" --yesno "$M" 8 75

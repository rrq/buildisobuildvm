#!/bin/zsh
#
# This script prepares a CD builder virtual machine image using
# debootstrap and selective file preparations for a debian-installer
# build within the VM.

if [ -z "$2" ] ; then
    echo "Usage: <dist> <arch>"
    exit 1
fi

if [ -z "$NOLOG" ] ; then
    NOLOG=bootstrap-log.$(date +%Y%m%d%H%M%S)
    NOLOG=$NOLOG $0 $* |& tee $NOLOG
    echo "log file: $NOLOG"
    exit 0
fi

DIST=$1
ARCH=$2
PROXY=http://10.10.10.1:3142 # my apt-cacher-ng proxy
SHARE=10.10.10.1:/home/ralph/kvm/ISO/share # nfs share for debian-installer
SHARETYPE=nfs
SOURCE=http://deb.devuan.org/merged
WHO=$USER
WHOID=$(id -u $USER)
TARGET=$DIST-$ARCH
EXCLUDE="fdisk iptables libsystemd0 bootlogd"
VARIANT="variant=minbase no-merged-usr"
    
# Let user in on changing some variables
. ./bootstrap-dialog.sh || exit 1

set -x

EXCLUDE=( ${(s[ ])EXCLUDE} )
VARIANT=( ${${(s[ ])VARIANT}/#/--} )

INCLUDE=( )
DISK=${DISK-$TARGET.img}

# Fix up arguments for debootstrap
VARIANT=( $VARIANT --arch $ARCH )
[ -z "$PROXY" ] || PPROXY=http_proxy=$PROXY

## Prepare the builder VM
./clean-disk.sh $DISK 2G 0 || exit 1
sudo rm -rf $TARGET || exit 1
mkdir -p $TARGET
LOOP=$(sudo losetup --show -f $DISK)
sudo mount ${LOOP}p1 $TARGET || exit 1

function alldone() {
    if mount | grep -q "on .*/$TARGET " ; then
	echo "Unmounting TARGET=$TARGET" >&2
	sudo umount $TARGET/proc 2> /dev/null
	sudo umount $TARGET/sys 2> /dev/null
	sudo umount $TARGET
    fi
    if sudo losetup -l $LOOP 2>/dev/null ; then
	echo "Detaching LOOP=$LOOP" >&2
	sudo losetup -d $LOOP
    fi
    exit
}
trap "alldone" 0 1 2 15

# default boot setup function
BOOT_SETUP=setup_extlinux_boot

## dist-arch set up
. ./build-$TARGET-target || exit 1

## run debootstrap
[ -z "EXCLUDE" ] || EXCLUDE=--exclude=${(j[,])EXCLUDE}
[ -z "INCLUDE" ] || INCLUDE=--include=${(j[,])INCLUDE}

echo "$PPROXY debootstrap $EXCLUDE $INCLUDE $VARIANT $DIST $TARGET $SOURCE"
sudo $PPROXY debootstrap $EXCLUDE $INCLUDE $VARIANT $DIST $TARGET $SOURCE 
[ -z "$DISK" ] && echo lost DISK && exit 1

# Install qemu-$ARCH-static
#FOREIGN=/usr/bin/qemu-$ARCH-static
#sudo cp -p /usr/bin/qemu-$ARCH-static $TARGET/usr/bin/ || exit 1

function setup_extlinux_boot() {
    echo "# install mbr boot loader to $DISK"
    MBR=$TARGET/usr/lib/EXTLINUX/altmbr.bin
    printf '\1' | cat $MBR - | dd of=$DISK bs=440 count=1 conv=notrunc

    echo "# Set up extlinux boot, hostname and automatic console login"
    cat <<EOF | sudo chroot $TARGET $FOREIGN /bin/bash
mkdir -p /boot/extlinux
cat <<END > /boot/extlinux/extlinux.conf
DEFAULT linux
  SAY $DIST-$ARCH starting ...
LABEL linux
  KERNEL /vmlinuz
  APPEND ro edd=off root=/dev/sda1 initrd=/initrd.img console=ttyS0,115200
END
mknod /dev/sda1 b $(lsblk --nodeps -noMAJ:MIN ${LOOP}p1 | tr : ' ')
ls -l /dev/sda1
/usr/bin/extlinux --install --device /dev/sda1 /boot/extlinux
rm -f /dev/sda1
echo "$TARGET" > /etc/hostname
echo "T3:23:respawn:/sbin/agetty -a root 115200 ttyS0" >> /etc/inittab
EOF
}

$BOOT_SETUP

[ "$BOOTSTRAPONLY" = y ] && exit 0

echo "# Install the $WHO ssh key if available"
if [ ! -z "$WHO" ] ; then
    WHOKEY=$(echo $(eval echo ~$WHO)/.ssh/id_rsa.pub)
    if [ -r $WHOKEY ] ; then
	cat <<EOF | sudo /bin/bash
mkdir -p $TARGET/root/.ssh
cp $WHOKEY $TARGET/root/.ssh/authorized_keys
chmod go= $TARGET/root/.ssh
EOF
    fi
fi

echo "# Add common extra packages"
POSTBOOT=" psmisc less nfs-client emacs-nox "
DIPKGS=" apt-utils dctrl-tools debhelper dpkg-dev fakeroot $EXTRA "

echo "# VM preparation as per bootstrap-egress.sh"
VARS=( $(egrep -o '\$[A-Z]+' bootstrap-egress.sh | sort -u ) )
export "${(@)VARS#$}"
sudo cp bootstrap-egress.sh $TARGET/root/bootstrap-egress.sh
sudo chmod a+x $TARGET/root/bootstrap-egress.sh
sudo -E chroot $TARGET $FOREIGN /bin/bash /root/bootstrap-egress.sh

echo "Completed set up of $DISK"
echo "run: ./qemu.sh $ARCH $DISK"
